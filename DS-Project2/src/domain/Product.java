package domain;

import java.util.ArrayList;

public class Product {
	
	private String userName;
	
	private ArrayList<Contact> contacts;
	
	private ArrayList<Warning> warnings;
	
	public Product() {
		this.contacts = new ArrayList<Contact>();
		this.warnings = new ArrayList<Warning>();
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the contacts
	 */
	public ArrayList<Contact> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return the warnings
	 */
	public ArrayList<Warning> getWarnings() {
		return warnings;
	}

	/**
	 * @param warnings the warnings to set
	 */
	public void setWarnings(ArrayList<Warning> warnings) {
		this.warnings = warnings;
	}
	
	
}
