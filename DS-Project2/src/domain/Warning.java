/**
 * 
 */
package domain;

import static language.Messages.BEGIN_DATE;
import static language.Messages.END_DATE;
import static language.Messages.MESSAGE;
import static language.Messages.PERIODICITY_STRING;
import static language.Messages.TIME_PERIODICITY;

import java.util.Date;

import business.WarningBO;
import language.I18N;

/**
 * @author pedro.martins
 *
 */
public class Warning {

	private int id;
	
	private String message;
	
	private Date beginDate;
	
	private Date endDate;
	
	private int timePeriodicity;
	
	private PeriodicityEnum periodicity;

	public Warning() {
	}
	
	public Warning(int id, String message, Date beginDate, Date endDate, int timePeriodicity, PeriodicityEnum periodicity) {
		this.id = id;
		this.message = message;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.timePeriodicity = timePeriodicity;
		this.periodicity = periodicity;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the beginDate
	 */
	public Date getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the timePeriodicity
	 */
	public int getTimePeriodicity() {
		return timePeriodicity;
	}

	/**
	 * @param timePeriodicity the timePeriodicity to set
	 */
	public void setTimePeriodicity(int timePeriodicity) {
		this.timePeriodicity = timePeriodicity;
	}

	/**
	 * @return the periodicity
	 */
	public PeriodicityEnum getPeriodicity() {
		return periodicity;
	}

	/**
	 * @param periodicity the periodicity to set
	 */
	public void setPeriodicity(PeriodicityEnum periodicity) {
		this.periodicity = periodicity;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Warning other = (Warning) obj;
	    return other.id==id &&
                other.message.equals(message) &&
                other.beginDate.equals(beginDate) &&
                other.endDate.equals(endDate) &&
                other.timePeriodicity==timePeriodicity &&
                other.periodicity.equals(periodicity);
	}
	
	@Override
	public int hashCode() {
		int result = 17;
        result = 31 * result + message.hashCode();
        result = 31 * result + id;
        result = 31 * result + beginDate.hashCode();
        result = 31 * result + endDate.hashCode();
        result = 31 * result + timePeriodicity;
        result = 31 * result + periodicity.hashCode();
        return result;
	}
	
	@Override
	public String toString() {
		return "\t" + "ID " + id + "\t" + I18N.getString(MESSAGE) + " " + message + "\t" + I18N.getString(BEGIN_DATE) + " " + WarningBO.sdf.format(beginDate) + 
				"\t" + I18N.getString(END_DATE) + " " + WarningBO.sdf.format(endDate) + "\t" + I18N.getString(TIME_PERIODICITY) + " " + timePeriodicity + " " + 
				I18N.getString(PERIODICITY_STRING) + " " + timePeriodicity + " " +  periodicity.name().toLowerCase();
	}

}
