package domain;

public enum PeriodicityEnum {
	SECONDS,
	MINUTES,
	HOURS,
	DAYS
}
