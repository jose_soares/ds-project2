package domain;

import static language.Messages.EMAIL;
import static language.Messages.NAME;
import static language.Messages.PHONE_NUMBER;

import language.I18N;

public class Contact{
	
	private int id;
	
	private int phoneNumber;
	
	private String name;
	
	private String email;
	
	public Contact() {
	}
	
	public Contact(int id, int phoneNumber, String name, String email) {
		this.id=id;
		this.phoneNumber = phoneNumber;
		this.name = name;
		this.email = email;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the phoneNumber
	 */
	public int getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "\t" + "ID " + id + "\t" + I18N.getString(NAME) + " " + name + "\t" + I18N.getString(EMAIL) + " " + email + "\t" + 
				I18N.getString(PHONE_NUMBER) + " " + phoneNumber;
	}

}
