package business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import domain.Contact;
import interfaces.ContactInterface;

public class ContactBO implements ContactInterface{

	File contactListFile;
	List<Contact> contactList;
	
	public ContactBO(ProductBO productBO) {
		contactList = productBO.getProduct().getContacts();
	}

	@Override
	public void loadContacts(File contactListFile) throws IOException {
		this.contactListFile = contactListFile;
		FileReader reader = new FileReader(contactListFile);
		BufferedReader bufferedReader = new BufferedReader(reader);

		String line;
		List<String> contacts = new ArrayList<>();

		while ((line = bufferedReader.readLine()) != null) {
			contacts.add(line);
		}
		reader.close();

		for (String contact : contacts) {
			String[] contactInfo = contact.split(",");
			Contact createContact = new Contact();
			createContact.setId(Integer.valueOf(contactInfo[0]));
			createContact.setName(contactInfo[1]);
			createContact.setEmail(contactInfo[2]);
			createContact.setPhoneNumber(Integer.valueOf(contactInfo[3]));
			contactList.add(createContact);
		}
	}

	@Override
	public void createContact(Contact c) throws IOException {

		contactList.add(c);
		
		addContactToFile(c);

	}

	@Override
	public void addContactToFile(Contact contactToSave) throws IOException {
		String lineToInsert;
		FileWriter fw = new FileWriter(contactListFile, true);
		lineToInsert = contactToSave.getId() + "," + contactToSave.getName() + "," + contactToSave.getEmail() + ","
				+ contactToSave.getPhoneNumber() + "\n";
		fw.write(lineToInsert);
		fw.close();
	}

	@Override
	public List<Contact> getListContacts() {
		return contactList;		
	}

	@Override
	public void updateContactsFile() throws IOException {	
		String contact;
		FileWriter fw=new FileWriter(contactListFile, false);
		for(Contact contactToSave : contactList) {
			contact = contactToSave.getId() + "," + contactToSave.getName() + "," + contactToSave.getEmail() + "," + contactToSave.getPhoneNumber() + "\n";
			fw.write(contact);
		}
		fw.close();
	}

	@Override
	public void deleteContact(int id)  throws IOException{
		if(id >= 0 && id <= contactList.size() -1) {
			contactList.remove(id);
			for(int i = 0 ; i < contactList.size() ; i++) {
				contactList.get(i).setId(i);
			}
			updateContactsFile();
		}else
			throw new IOException();
	}

	
}
