package business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import domain.PeriodicityEnum;
import domain.Warning;
import interfaces.WarningInterface;


public class WarningBO implements WarningInterface {

	private ProductBO productBO;
	public static SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private File warningListFile;
	private List<Warning> warningList;
	private Scheduler scheduler;
	
	public WarningBO(ProductBO productBO) {
		this.productBO = productBO;
		this.warningList = productBO.getProduct().getWarnings();
		this.scheduler = new Scheduler(this);
	}
	
	@Override
	public void loadWarnings(File warningListFile) throws Exception {
		this.warningListFile=warningListFile;
		FileReader reader = new FileReader(warningListFile);
		BufferedReader bufferedReader = new BufferedReader(reader);

		String line;
		List<String> warnings = new ArrayList<>();

		while ((line = bufferedReader.readLine()) != null) {
			warnings.add(line);
		}
		reader.close();

		for (String warning : warnings) {
			String[] warningInfo = warning.split(",");
			Warning createWarning = new Warning();
			createWarning.setId(Integer.valueOf(warningInfo[0]));
			createWarning.setMessage(warningInfo[1]);
			createWarning.setBeginDate(sdf.parse(warningInfo[2]));
			createWarning.setEndDate(sdf.parse(warningInfo[3]));
			createWarning.setTimePeriodicity(Integer.valueOf(warningInfo[4]));
			createWarning.setPeriodicity(PeriodicityEnum.valueOf(warningInfo[5]));
			warningList.add(createWarning);
			scheduler.sendWarnings(productBO.getProduct().getContacts(), createWarning);
		}
	}

	@Override
	public void createWarning(Warning w) throws IOException {

		warningList.add(w);
		
		addWarningToFile(w);

		scheduler.sendWarnings(productBO.getProduct().getContacts(), w);
	}

	@Override
	public void addWarningToFile(Warning warningToSave) throws IOException {
		String lineToInsert;
		FileWriter fw = new FileWriter(warningListFile, true);
		lineToInsert = warningToSave.getId() + "," + warningToSave.getMessage() + "," + sdf.format(warningToSave.getBeginDate()) + "," + 
				sdf.format(warningToSave.getEndDate()) + "," + warningToSave.getTimePeriodicity() + "," + 
				warningToSave.getPeriodicity().name() + "\n";
		fw.write(lineToInsert);
		fw.close();
	}

	@Override
	public List<Warning> getListWarnings() {
		return warningList;		
	}

	@Override
	public void updateWarningsFile() throws IOException {	
		String warning;
		FileWriter fw=new FileWriter(warningListFile, false);
		for(Warning warningToSave : warningList) {
			warning = warningToSave.getId() + "," + warningToSave.getMessage() + "," + warningToSave.getBeginDate() + "," + warningToSave.getEndDate() + 
					"," + warningToSave.getTimePeriodicity() + "," + warningToSave.getPeriodicity() +"\n";
			fw.write(warning);
		}
		fw.close();
	}

	public void removeWarningAlreadyDone(Warning w) throws IOException{
		warningList.remove(w.getId());
		for(int i = 0 ; i < warningList.size() ; i++) {
			warningList.get(i).setId(i);
		}
		updateWarningsFile();
	}
	
	@Override
	public void deleteWarning(int id)  throws IOException{
		Warning warningToRemove;
		if(id >= 0 && id <= warningList.size() -1) {
			warningToRemove = warningList.get(id);
			warningList.remove(id);
			for(int i = 0 ; i < warningList.size() ; i++) {
				warningList.get(i).setId(i);
			}
			updateWarningsFile();
			scheduler.cancelWarning(warningToRemove);
		}else
			throw new IOException();
	}

	@Override
	public SimpleDateFormat getSDF() {
		return sdf;
	}

	@Override
	public PeriodicityEnum resolvePeriodicityEnum(String periodicity) throws Exception {
		switch (periodicity) {
		case "SECONDS":
			return PeriodicityEnum.SECONDS;
		case "SEGUNDOS":
			return PeriodicityEnum.SECONDS;
		case "MINUTES":
			return PeriodicityEnum.MINUTES;
		case "MINUTOS":
			return PeriodicityEnum.MINUTES;
		case "HOURS":
			return PeriodicityEnum.HOURS;
		case "HORAS":
			return PeriodicityEnum.HOURS;
		case "DAYS":
			return PeriodicityEnum.DAYS;
		case "DIAS":
			return PeriodicityEnum.DAYS;
		default:
			throw new Exception();
		}
	}
	
}
