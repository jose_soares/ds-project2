/**
 * 
 */
package business;

import static language.Messages.PHONE_NUMBER;
import static language.Messages.TIME;
import static language.Messages.WARNING_MESSAGE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import domain.Contact;
import domain.Warning;
import language.I18N;

/**
 * @author pedro.martins
 *
 */

public class Scheduler {

	private WarningBO warningBO;
	private final ScheduledExecutorService scheduler;

	private Map<Warning, ScheduledFuture<?>> warningMap;

	public Scheduler(WarningBO warningBO) {
		 scheduler = Executors.newScheduledThreadPool(6);
		 warningMap = new HashMap<>();
		 this.warningBO=warningBO;
	}
	
	public void sendWarnings(ArrayList<Contact> contacts, Warning warning) throws IOException {
		if (warning.getEndDate().getTime() - System.currentTimeMillis() > 0) {
			final Runnable warningMessage = new Runnable() {
				public void run() {
					contactWarnningRunner(contacts, warning);
				}
			};
		
			final ScheduledFuture<?> warningHandle = scheduler.scheduleAtFixedRate(warningMessage,
					warning.getBeginDate().getTime() - System.currentTimeMillis(),
					getTimeUnitsOfWarning(warning), TimeUnit.MILLISECONDS);
			
			warningMap.put(warning, warningHandle);
			
			scheduler.schedule(new Runnable() {
				public void run() {
					warningHandle.cancel(false);
//					Automaticaly deletes a warning when its complete
//					try {
//						warningBO.removeWarningAlreadyDone(warning);
//					} catch (IOException e) {
//						System.out.println("\n\t " + I18N.getString(ERROR));
//					}
				}
			}, warning.getEndDate().getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		}
//		to delete already passed warnings
//		else {	
//			warningBO.removeWarningAlreadyDone(warning);
//			//System.out.println("Apaguei o warning com id " + warning.getId());
//		}
	}
	
	private void contactWarnningRunner(ArrayList<Contact> contacts, Warning warning) {
		for (Contact contact : contacts)
			System.out.println(I18N.getString(WARNING_MESSAGE) + " " + warning.getMessage() + " " + I18N.getString(PHONE_NUMBER) 
			+ " " + contact.getPhoneNumber() + " " + I18N.getString(TIME) + " " + warningBO.getSDF().format(System.currentTimeMillis()));
	}
	
	public void cancelWarning(Warning warning) {
    	warningMap.get(warning).cancel(false);
    }
    
	long getTimeUnitsOfWarning(Warning w) {
		switch (w.getPeriodicity()) {
			case SECONDS:
				return w.getTimePeriodicity() * 1000;
			case MINUTES:
				return w.getTimePeriodicity() * 60000;
			case HOURS:
				return w.getTimePeriodicity() * 3600000;
			case DAYS:
				return w.getTimePeriodicity() * 86400000;
			default:
				return w.getTimePeriodicity() * 1000;
		}
	}
    
}
