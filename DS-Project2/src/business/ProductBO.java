package business;

import static language.Messages.ERROR;

import java.io.File;
import java.io.IOException;

import domain.Product;
import interfaces.ProductInterface;
import language.I18N;


public class ProductBO implements ProductInterface{

	private Product product;
	private static String productFolderName;
	private ContactBO contactBO;
	private WarningBO warningBO;
	private File contactListFile;
	private File warningListFile;
	private String productDir;
	
	public ProductBO() {
		product = new Product();
		this.contactBO = new ContactBO(this);
		this.warningBO = new WarningBO(this);
	}
	
	/**
	 * @return the contactBO
	 */
	public ContactBO getContactBO() {
		return contactBO;
	}

	/**
	 * @return the warningBO
	 */
	public WarningBO getWarningBO() {
		return warningBO;
	}

	@Override
	public void setupProduct() {
		productDir = System.getProperty("user.dir");
		
		productDir += "//files//" + productFolderName;
		
		File file = new File(productDir);
        if (!file.exists() || !file.isDirectory()) {
            if (!file.mkdirs()) {
            	System.out.println(I18N.getString(ERROR));
            }	
        }
		
		contactListFile = new File(productDir + "//contacts.txt");
		if(!contactListFile.exists()) {
			try {
				contactListFile.createNewFile();
			} catch (IOException e) {
				System.out.println(I18N.getString(ERROR));
			}	
		}
		
		try {
			contactBO.loadContacts(contactListFile);
		} catch (IOException e) {
			System.out.println("\n\t " + I18N.getString(ERROR));//Error reading file
		}
		
		warningListFile = new File(productDir + "//warnings.txt");
		if(!warningListFile.exists()) {
			try {
				warningListFile.createNewFile();
			} catch (IOException e) {
				System.out.println(I18N.getString(ERROR));
			}	
		}
		
		try {
			warningBO.loadWarnings(warningListFile);
		} catch (Exception e) {
			System.out.println("\n\t " + I18N.getString(ERROR));//Error reading file
		}
		
	}

	@Override
	public Product getProduct() {
		return product;
	}
	
	/**
	 * @param productFolderName the productFolderName to set
	 */
	public static void setProductFolderName(String productToSet) {
		productFolderName = productToSet;
	}
}
