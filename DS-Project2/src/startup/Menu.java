package startup;

import static language.Messages.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

import business.ContactBO;
import business.ProductBO;
import business.WarningBO;
import domain.Contact;
import domain.PeriodicityEnum;
import domain.Warning;
import language.I18N;

public class Menu {
	
	private ProductBO productBO;
	private ContactBO contactBO;
	private WarningBO warningBO;
	
	Scanner sc = new Scanner(System.in);
	
	public Menu() {
		sc.useDelimiter("\n");
		productBO = new ProductBO();
		contactBO = productBO.getContactBO();
		warningBO = productBO.getWarningBO();
		productBO.setupProduct();
	};
		
	public void mainMenu() {

		while (true) {
			System.out.println("\n\t " + I18N.getString(MENU));
			System.out.println("1 - " + I18N.getString(CREATE_CONTACT));
			System.out.println("2 - " + I18N.getString(LIST_CONTACTS));
			System.out.println("3 - " + I18N.getString(DELETE_CONTACT));
		    System.out.println("4 - " + I18N.getString(CREATE_WARNING));
		    System.out.println("5 - " + I18N.getString(LIST_WARNING));
		    System.out.println("6 - " + I18N.getString(DELETE_WARNING));
		    System.out.println("7 - " + I18N.getString(END_PROGRAM));
			
			System.out.println(I18N.getString(CHOOSE));
			switch (sc.nextLine()) {
				case "1":// Create contact
					createContact();
					break;
	
				case "2":// List contacts
					listContacts();
					break;
	
				case "3":// Delete contact
					deleteContact();
					break;
	
				case "4":// Create warning
					createWarning();
					break;
	
				case "5":// List warnings
					listWarnings();
					break;
	
				case "6":// Delete warning
					deleteWarning();
					break;
	
				case "7": // Terminate program
					System.exit(1);
					break;
	
				default:
					System.out.println(I18N.getString(INVALID));
			}
		
		}
	}

	private void deleteContact() {
		if(contactBO.getListContacts().size()==0) {
			System.out.println(I18N.getString(NO_CONTACTS));
		}
		else {
			String idString;
			int id;
			listContacts();
			System.out.println(I18N.getString(CONTACT_TO_DELETE));
			while(true) {
				idString = sc.nextLine();
				if(idString.toUpperCase().equals("C"))
					mainMenu();
				try {
					id = Integer.valueOf(idString);
					contactBO.deleteContact(id);
					break;
				}catch (Exception e){
					System.out.println(I18N.getString(CANT_DELETE_CONTACT));
				}
			}
			System.out.println(I18N.getString(CONTACT_DELETED));
			listContacts();
		}
	}
	
	private void deleteWarning() {
		if(warningBO.getListWarnings().size()==0) {
			System.out.println(I18N.getString(NO_WARNINGS));
		}
		else {
			String idString;
			int id;
			listWarnings();
			System.out.println(I18N.getString(WARNING_TO_DELETE));
			while(true) {
				idString = sc.nextLine();
				if(idString.toUpperCase().equals("C"))
					mainMenu();
				try {
					id = Integer.valueOf(idString);
					warningBO.deleteWarning(id);
					break;
				}catch (Exception e){
					System.out.println(I18N.getString(CANT_DELETE_CONTACT));
				}
			}

			System.out.println(I18N.getString(WARNING_DELETED));
			listWarnings();
		}
	}
	
	private void listContacts() {
		if(contactBO.getListContacts().size()==0) {
			System.out.println(I18N.getString(NO_CONTACTS));
		}
		else { 
			System.out.println(I18N.getString(CONTACTS_LIST));
			for(Contact c : contactBO.getListContacts()) {
				System.out.println(c.toString() + "\n");
			}
		}
	}

	private void listWarnings() {
		if(warningBO.getListWarnings().size()==0) {
			System.out.println(I18N.getString(NO_WARNINGS));
		}
		else { 
			System.out.println(I18N.getString(WARNINGS_LIST));
			for(Warning w : warningBO.getListWarnings()) {
				System.out.println(w.toString() + "\n");
			}
		} 
	}
	
	private void createContact() {
		String pnumberString;
		int pnumber;
		String name;
		String email;
		System.out.println(I18N.getString(CREATING_CONTACT));
		
		System.out.println(I18N.getString(INS_PHONE));
		
		while(true) {
			try {
				pnumberString = sc.nextLine();
				if(pnumberString.length()!=9)
					throw new NumberFormatException();
				pnumber = Integer.valueOf(pnumberString);
				break;
			} catch (NumberFormatException e) {
				System.out.println(I18N.getString(INVALID_NUMBER_TRY_AGAIN));
			}	
		}
		
		System.out.println(I18N.getString(INS_NAME));
		name = sc.nextLine();
		
		System.out.println(I18N.getString(INS_EMAIL));
		email = sc.nextLine();
		
		Contact c = new Contact(contactBO.getListContacts().size(), pnumber, name, email);
		try {
			contactBO.createContact(c);
		} catch (IOException e) {
			System.out.println("\n\t " + I18N.getString(ERROR));// Error reading file
		}
				
		System.out.println(I18N.getString(CONTACT_ADDED));
		
	}

	private void createWarning() {
		String warningMessage, beginDateString, endDateString, periodicity, periodicityTimeString;
		int periodicityTime;
		Date beginDate, endDate;
		PeriodicityEnum periodicityEnum;
		
		System.out.println(I18N.getString(CREATE_WARNING));
		
		System.out.println(I18N.getString(WARNING_MESSAGE));
		warningMessage = sc.nextLine();
		
		System.out.println(I18N.getString(WARNING_BEGIN_DATE));
		while(true) {
			try {
				beginDateString = sc.nextLine();
				warningBO.getSDF().setLenient(false);
				beginDate = warningBO.getSDF().parse(beginDateString);
				break;
			} catch (ParseException e) {
				System.out.println(I18N.getString(INVALID_DATE_TRY_AGAIN));
			}
		}
		
		System.out.println(I18N.getString(WARNING_END_DATE));
		while(true) {
			try {
				endDateString = sc.nextLine();
				warningBO.getSDF().setLenient(false);
				endDate = warningBO.getSDF().parse(endDateString);
				if(beginDate.before(endDate))
					break;
				System.out.println(I18N.getString(INVALID_DATE_TRY_AGAIN));
			} catch (ParseException e) {
				System.out.println(I18N.getString(INVALID_DATE_TRY_AGAIN));
			}
		}
		
		System.out.println(I18N.getString(WARNING_PERIODICITY_TIME));
		while(true) {
			try {
				periodicityTimeString = sc.nextLine();
				periodicityTime = Integer.valueOf(periodicityTimeString);
				if(periodicityTime <= 0)
					throw new NumberFormatException();
				break;
			} catch (NumberFormatException e) {
				System.out.println(I18N.getString(INVALID_NUMBER_TRY_AGAIN));
			}		
		}
		
		System.out.println(I18N.getString(WARNING_PERIODICITY));
		while(true) {
			try {
				periodicity = sc.nextLine().toUpperCase();
				periodicityEnum = warningBO.resolvePeriodicityEnum(periodicity);
				break;
			} catch (Exception e) {
				System.out.println(I18N.getString(INVALID_PERIODICITY_TRY_AGAIN));
			}	
		}
		
		Warning w = new Warning(warningBO.getListWarnings().size(), warningMessage, beginDate, endDate, periodicityTime, periodicityEnum);

		try {
			warningBO.createWarning(w);
		} catch (IOException e) {
			System.out.println("\n\t " + I18N.getString(ERROR));// Error reading file
		}
			
		System.out.println(I18N.getString(WARNING_ADDED));
		
	}

}
