package startup;

import static language.Messages.STARTUP;
import static language.Messages.LOADING;

import language.I18N;

/**
 * IoT software product line for smart houses and houses with disable people.
 * Startup of the application desired (one out of the 4 predefined ones: 
 * "wearableDevWithActMonitor", "wearableDevWithButton", "AirQualitySensor", "....").
 * @author Jose Soares fc44844
 * @author Nuno Nelas fc51691
 * @author Pedro Martins fc52484
 * 
 */
public class Startup {

	public static void main(String[] args) {		
		
		System.out.println(I18N.getString(STARTUP));
		System.out.println(I18N.getString(LOADING));
		
		Menu menu = new Menu();
        menu.mainMenu();
	}
}
