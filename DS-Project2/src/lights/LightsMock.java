package lights;

import language.I18N;
import static language.Messages.LIGHTS_ON;
import static language.Messages.LIGHTS_OFF;

public class LightsMock {
	
	private static void lightOn() {
		System.out.println(I18N.getString(LIGHTS_ON));
	}
	
	private static void lightOff() {
		System.out.println(I18N.getString(LIGHTS_OFF));
	}
	
	public static void lightFlash() {
		lightOn();
		lightOff();
		lightOn();
		lightOff();
	}

}
