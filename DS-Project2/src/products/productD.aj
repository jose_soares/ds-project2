package products;

import static language.Messages.PRODUCT_D;

import business.ProductBO;
import language.I18N;

public aspect productD {
	
	before() : execution(* *.setupProduct()) {
		ProductBO.setProductFolderName(I18N.getString(PRODUCT_D));
	}
}
