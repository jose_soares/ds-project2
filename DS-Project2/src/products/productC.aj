package products;

import static language.Messages.PRODUCT_C;

import business.ProductBO;
import language.I18N;

public aspect productC {
	
	before() : execution(* *.setupProduct()) {
		ProductBO.setProductFolderName(I18N.getString(PRODUCT_C));
	}
}
