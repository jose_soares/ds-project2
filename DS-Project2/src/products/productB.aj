package products;

import static language.Messages.PRODUCT_B;

import business.ProductBO;
import language.I18N;

public aspect productB {
	
	before() : execution(* *.setupProduct()) {
		ProductBO.setProductFolderName(I18N.getString(PRODUCT_B));
	}
}
