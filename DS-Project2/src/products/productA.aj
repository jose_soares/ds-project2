package products;

import static language.Messages.PRODUCT_A;

import business.ProductBO;
import language.I18N;

public aspect productA {

	before() : execution(* *.setupProduct()) {
		ProductBO.setProductFolderName(I18N.getString(PRODUCT_A));
	}
}
