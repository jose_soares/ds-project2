package features;

import static language.Messages.START_AIRQUALITY_MON;
import static language.Messages.SELECT_ALERT_AIRQUALITY;
import static language.Messages.SELECT_HUMIDITY;
import static language.Messages.SELECT_DUST;
import static language.Messages.SELECT_POLLEN;
import static language.Messages.AIR_QUALITY;
import static language.Messages.HUMIDITY_MSG;
import static language.Messages.DUST_MSG;
import static language.Messages.POLLEN_MSG;
import static language.Messages.RECEIVED_UPDATE;

import java.util.Scanner;

import com.bezirk.middleware.Bezirk;
import com.bezirk.middleware.addressing.ZirkEndPoint;
import com.bezirk.middleware.java.proxy.BezirkMiddleware;
import com.bezirk.middleware.messages.Event;
import com.bezirk.middleware.messages.EventSet;

import events.AirQualityUpdateEvent;

import language.I18N;

/**
 * @author nuno.nelas
 *
 */

public aspect airQualityMonitor {

	private double minHumidity; 
	private int minDust, minPollen;

	pointcut cutMenu() : call(void startup.Menu.mainMenu());

	before() : cutMenu() {
		setup();
		subscribeAirQuality();
	}

	private void setup() {
		Scanner scanner = new Scanner(System.in);

		System.out.println("\n\t" + I18N.getString(START_AIRQUALITY_MON));
		System.out.println(I18N.getString(SELECT_ALERT_AIRQUALITY));
		System.out.println("\n" + I18N.getString(SELECT_HUMIDITY));
		minHumidity = scanner.nextDouble();
		System.out.println("\n"  + I18N.getString(SELECT_DUST));
		minDust = scanner.nextInt();
		System.out.println("\n" + I18N.getString(SELECT_POLLEN));
		minPollen = scanner.nextInt();
	}

	private void subscribeAirQuality() {
		BezirkMiddleware.initialize();
		Bezirk bezirk = BezirkMiddleware.registerZirk(I18N.getString(AIR_QUALITY));

		EventSet airQualityEvents = new EventSet(AirQualityUpdateEvent.class);

		airQualityEvents.setEventReceiver(new EventSet.EventReceiver() {
			@Override
			public void receiveEvent(Event event, ZirkEndPoint sender) {
				//Check if this event is of interest
				if (event instanceof AirQualityUpdateEvent) {
					AirQualityUpdateEvent aqUpdate = (AirQualityUpdateEvent) event;

					if ((aqUpdate.getHumidity() > minHumidity) || (aqUpdate.getDustLevel() > minDust) || (aqUpdate.getPollenLevel() > minPollen)) {
						System.err.println("\n" + I18N.getString(RECEIVED_UPDATE) + aqUpdate.toString());
					}

					//do something in response to this event
					if (aqUpdate.getHumidity() > minHumidity) {
						printHumidity();
					}
					
					if (aqUpdate.getDustLevel() > minDust) {
						printDust();
					}
					
					if (aqUpdate.getPollenLevel() > minPollen) {
						printPollen();
					}
				}
			}
		});
		bezirk.subscribe(airQualityEvents);
	}
	
	private void printHumidity(){
		System.out.println(I18N.getString(HUMIDITY_MSG));
	}
	
	private void printDust(){
		System.out.println(I18N.getString(DUST_MSG));
	}
	
	private void printPollen(){
		System.out.println(I18N.getString(POLLEN_MSG));
	}
}
