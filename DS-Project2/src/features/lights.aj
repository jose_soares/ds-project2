package features;

import java.util.ArrayList;

import domain.Contact;
import domain.Warning;
import events.MovementUpdateEvent;
import events.TemperatureUpdateEvent;
import lights.LightsMock;

public aspect lights {
	
	pointcut lightsWarning(ArrayList<Contact> contacts, Warning warning) : 
		execution (private void *.contactWarnningRunner(ArrayList<Contact>, Warning)) && 
		args(contacts, warning);
	
	before(ArrayList<Contact> contacts, Warning warning) : 
		lightsWarning(contacts, warning) {
			LightsMock.lightFlash();
		}
	
	pointcut lightsTemperature(TemperatureUpdateEvent tUpdate) : 
		execution (private void *.printTemperature(TemperatureUpdateEvent)) && 
		args(tUpdate);
	
	before(TemperatureUpdateEvent tUpdate) : 
		lightsTemperature(tUpdate) {
			LightsMock.lightFlash();
		}
	
	pointcut lightsMovement(MovementUpdateEvent mUpdate) : 
		execution (private void *.printMovements(MovementUpdateEvent)) && 
		args(mUpdate);
	
	before(MovementUpdateEvent mUpdate) : 
		lightsMovement(mUpdate) {
			LightsMock.lightFlash();
		}
	
	pointcut lightsAirQualityHumidity() : 
		execution (private void *.printHumidity());
	
	before() : 
		lightsAirQualityHumidity() {
			LightsMock.lightFlash();
		}
	
	pointcut lightsAirQualityDust() : 
		execution (private void *.printDust());
	
	before() : 
		lightsAirQualityDust() {
			LightsMock.lightFlash();
		}
	
	pointcut lightsAirQualityPollen() : 
		execution (private void *.printPollen());
	
	before() : 
		lightsAirQualityPollen() {
			LightsMock.lightFlash();
		}
}
