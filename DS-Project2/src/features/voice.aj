package features;

import static language.Messages.DUST_MSG;
import static language.Messages.TEMPERATURE_MSG;
import static language.Messages.HUMIDITY_MSG;
import static language.Messages.POLLEN_MSG;
import static language.Messages.MOVEMENT_DETECTED;

import java.util.ArrayList;

import domain.Contact;
import domain.Warning;
import events.MovementUpdateEvent;
import events.TemperatureUpdateEvent;
import language.I18N;
import voice.Voice;

public aspect voice {
	
	pointcut voiceWarning(ArrayList<Contact> contacts, Warning warning) : 
		execution (private void *.contactWarnningRunner(ArrayList<Contact>, Warning)) && 
		args(contacts, warning);
	
	before(ArrayList<Contact> contacts, Warning warning) : 
		voiceWarning(contacts, warning) {
			Voice.talk(warning.getMessage());
		}
	
	pointcut voiceTemperature(TemperatureUpdateEvent tUpdate) : 
		execution (private void *.printTemperature(TemperatureUpdateEvent)) && 
		args(tUpdate);
	
	before(TemperatureUpdateEvent tUpdate) : 
		voiceTemperature(tUpdate) {
			Voice.talk(I18N.getString(TEMPERATURE_MSG));
		}
	
	pointcut voiceMovement(MovementUpdateEvent mUpdate) : 
		execution (private void *.printMovements(MovementUpdateEvent)) && 
		args(mUpdate);
	
	before(MovementUpdateEvent mUpdate) : 
		voiceMovement(mUpdate) {
			Voice.talk(I18N.getString(MOVEMENT_DETECTED));
		}
	
	pointcut voiceAirQualityHumidity() : 
		execution (private void *.printHumidity());
	
	before() : 
		voiceAirQualityHumidity() {
			Voice.talk(I18N.getString(HUMIDITY_MSG));
		}
	
	pointcut voiceAirQualityDust() : 
		execution (private void *.printDust());
	
	before() : 
		voiceAirQualityDust() {
			Voice.talk(I18N.getString(DUST_MSG));
		}
	
	pointcut voiceAirQualityPollen() : 
		execution (private void *.printPollen());
	
	before() : 
		voiceAirQualityPollen() {
			Voice.talk(I18N.getString(POLLEN_MSG));
		}
}
