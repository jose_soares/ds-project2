package features;

import static language.Messages.TEMPERATURE;
import static language.Messages.START_TEMPERATURE_MON;
import static language.Messages.SELECT_ALERT_TEMPERATURE;
import static language.Messages.SELECT_TEMPERATURE;
import static language.Messages.TEMPERATURE_MSG;
import static language.Messages.RECEIVED_UPDATE;

import java.util.Scanner;

import com.bezirk.middleware.Bezirk;
import com.bezirk.middleware.addressing.ZirkEndPoint;
import com.bezirk.middleware.java.proxy.BezirkMiddleware;
import com.bezirk.middleware.messages.Event;
import com.bezirk.middleware.messages.EventSet;

import events.TemperatureUpdateEvent;
import language.I18N;

/**
 * @author nuno.nelas
 *
 */

public aspect temperatureMonitor {

	private int minTemperature;
	
	pointcut cutMenu() : call(void startup.Menu.mainMenu());

	before() : cutMenu() {
		setup();
		subscribeTemperature();
	}

	private void setup() {
		Scanner scanner = new Scanner(System.in);

		System.out.println("\n\t" + I18N.getString(START_TEMPERATURE_MON));
		System.out.println(I18N.getString(SELECT_ALERT_TEMPERATURE));
		System.out.println("\n" + I18N.getString(SELECT_TEMPERATURE));
		minTemperature = scanner.nextInt();
	}

	private void subscribeTemperature() {
		BezirkMiddleware.initialize();
		Bezirk bezirk = BezirkMiddleware.registerZirk(I18N.getString(TEMPERATURE));

		EventSet temperatureEvents = new EventSet(TemperatureUpdateEvent.class);

		temperatureEvents.setEventReceiver(new EventSet.EventReceiver() {
			@Override
			public void receiveEvent(Event event, ZirkEndPoint sender) {
				//Check if this event is of interest
				if (event instanceof TemperatureUpdateEvent) {
					TemperatureUpdateEvent tUpdate = (TemperatureUpdateEvent) event;

					//do something in response to this event
					if (tUpdate.getTemperature() > minTemperature) {
						printTemperature(tUpdate);
					}
				}
			}
		});
		bezirk.subscribe(temperatureEvents);
	}
	
	private void printTemperature(TemperatureUpdateEvent tUpdate) {
		System.err.println("\n" + I18N.getString(RECEIVED_UPDATE) + String.valueOf(tUpdate.getTemperature()));
		System.out.println(I18N.getString(TEMPERATURE_MSG));
	}


}
