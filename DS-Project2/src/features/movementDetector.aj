package features;

import static language.Messages.MOVEMENT;
import static language.Messages.START_MOVEMENT_MON;
import static language.Messages.SELECT_ALERT_MOVEMENT;
import static language.Messages.SELECT_START_MOVEMENT;
import static language.Messages.SELECT_FINISH_MOVEMENT;
import static language.Messages.MOVEMENT_DETECTED;
import static language.Messages.RECEIVED_UPDATE;

import java.util.Scanner;

import com.bezirk.middleware.Bezirk;
import com.bezirk.middleware.addressing.ZirkEndPoint;
import com.bezirk.middleware.java.proxy.BezirkMiddleware;
import com.bezirk.middleware.messages.Event;
import com.bezirk.middleware.messages.EventSet;

import events.MovementUpdateEvent;
import language.I18N;

/**
 * @author nuno.nelas
 *
 */

public aspect movementDetector {

	private int minHour, maxHour;

	pointcut cutMenu() : call(void startup.Menu.mainMenu());

	before() : cutMenu() {
		setup();
		subscribeMovements();
	}

	private void setup() {
		Scanner scanner = new Scanner(System.in);

		System.out.println("\n\t" + I18N.getString(START_MOVEMENT_MON));
		System.out.println(I18N.getString(SELECT_ALERT_MOVEMENT));
		System.out.println("\n" + I18N.getString(SELECT_START_MOVEMENT));
		minHour = scanner.nextInt();
		System.out.println("\n" + I18N.getString(SELECT_FINISH_MOVEMENT));
		maxHour = scanner.nextInt();
	}

	private void subscribeMovements() {
		BezirkMiddleware.initialize();
		Bezirk bezirk = BezirkMiddleware.registerZirk(I18N.getString(MOVEMENT));

		EventSet movementEvents = new EventSet(MovementUpdateEvent.class);

		movementEvents.setEventReceiver(new EventSet.EventReceiver() {
			@Override
			public void receiveEvent(Event event, ZirkEndPoint sender) {
				//Check if this event is of interest
				if (event instanceof MovementUpdateEvent) {
					MovementUpdateEvent mUpdate = (MovementUpdateEvent) event;

					//do something in response to this event
					if (mUpdate.getTime().getHours() >= minHour || mUpdate.getTime().getHours() < maxHour) {
						printMovements(mUpdate);
					}

				}
			}
		});
		bezirk.subscribe(movementEvents);
	}
	
	private void printMovements(MovementUpdateEvent mUpdate) {
		System.err.println("\n" + I18N.getString(RECEIVED_UPDATE) + mUpdate.getTime());
		System.out.println(I18N.getString(MOVEMENT_DETECTED));
	}
}
