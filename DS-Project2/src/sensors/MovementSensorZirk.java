package sensors;

import static language.Messages.DEVICE_RUNNING;

import java.util.Date;

import com.bezirk.middleware.Bezirk;
import com.bezirk.middleware.java.proxy.BezirkMiddleware;

import events.MovementUpdateEvent;
import language.I18N;

/**
 * @author nuno.nelas
 *
 */

public class MovementSensorZirk {

	private Bezirk bezirk;

	public MovementSensorZirk() {
		BezirkMiddleware.initialize();
		bezirk = BezirkMiddleware.registerZirk("Movement Sensor Zirk");
		System.err.println("Got Bezirk instance");
	}

	public void sendMovementUpdate() {
		//produces some  values since this is a mock
		final Date time = new Date();

		final MovementUpdateEvent movementUpdateEvent = new MovementUpdateEvent(time);

		//sends the event
		bezirk.sendEvent(movementUpdateEvent);
		System.err.println("Movement detected: " + movementUpdateEvent.toString());
	}

	public static void main(String args[]) throws InterruptedException {
		MovementSensorZirk movementSensorZirk = new MovementSensorZirk();
		System.err.println("This product has Movement Sensor");

		System.out.println(I18N.getString(DEVICE_RUNNING, "Movement Sensor"));
		movementSensorZirk.sendMovementUpdate();
	}
}
