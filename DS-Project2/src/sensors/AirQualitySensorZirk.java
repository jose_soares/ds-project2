package sensors;

import language.I18N;
import static language.Messages.DEVICE_RUNNING;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.bezirk.middleware.Bezirk;
import com.bezirk.middleware.java.proxy.BezirkMiddleware;

/**
 * @author nuno.nelas
 *
 */

import events.AirQualityUpdateEvent;

public class AirQualitySensorZirk{
	private Bezirk bezirk;

	public AirQualitySensorZirk() {
		BezirkMiddleware.initialize();
		bezirk = BezirkMiddleware.registerZirk("Air Quality Sensor Zirk");
		System.err.println("Got Bezirk instance");
	}

	public void sendAirQualityUpdate(double humidity, int dustLevel, int pollenLevel) {
		final AirQualityUpdateEvent airQualityUpdateEvent = new AirQualityUpdateEvent(humidity, dustLevel, pollenLevel);

		//sends the event
		bezirk.sendEvent(airQualityUpdateEvent);
		System.err.println("Published air quality update: " + airQualityUpdateEvent.toString());
	}

	public static void main(String args[]) throws InterruptedException {
		AirQualitySensorZirk airQualitySensorZirk = new AirQualitySensorZirk();
		System.err.println("This product has an Air Quality Sensor");

		System.out.println(I18N.getString(DEVICE_RUNNING, "Air Quality Sensor"));

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				Random humRandom = new Random();
				double humidity = (humRandom.nextInt((9 - 1) + 1) + 1) / 10.0;
				
				Random dustRandom = new Random();
				int dustLevel = dustRandom.nextInt((30 - 1) + 1) + 1;
				
				Random pollenRandom = new Random();
				int pollenLevel = pollenRandom.nextInt((700 - 300) + 1) + 300;
				airQualitySensorZirk.sendAirQualityUpdate(humidity, dustLevel, pollenLevel);
			}
		}, 0, 30*1000);
	}

}