package sensors;

import static language.Messages.DEVICE_RUNNING;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.bezirk.middleware.Bezirk;
import com.bezirk.middleware.java.proxy.BezirkMiddleware;
import events.TemperatureUpdateEvent;
import language.I18N;

/**
 * @author nuno.nelas
 *
 */

public class TemperatureSensorZirk {
	private Bezirk bezirk;

	public TemperatureSensorZirk() {
		BezirkMiddleware.initialize();
		bezirk = BezirkMiddleware.registerZirk("Temperature Sensor Zirk");
		System.err.println("Got Bezirk instance");
	}

	public void sendTemperatureUpdate(double temperature) {
		final TemperatureUpdateEvent temperatureUpdateEvent = new TemperatureUpdateEvent(temperature);

		//sends the event
		bezirk.sendEvent(temperatureUpdateEvent);
		System.err.println("Published temperature update: " + temperature);
	}

	public static void main(String args[]) throws InterruptedException {
		TemperatureSensorZirk temperatureSensorZirk = new TemperatureSensorZirk();
		System.err.println("This product has a temperature Sensor");

		System.out.println(I18N.getString(DEVICE_RUNNING, "Temperature Sensor"));
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				Random tempRandom = new Random();
				double temperature = tempRandom.nextInt((120 - 20) + 1) + 20;
				temperatureSensorZirk.sendTemperatureUpdate(temperature);
			}
		}, 0, 30*1000);
	}
}
