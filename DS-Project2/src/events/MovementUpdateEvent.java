package events;

import static language.Messages.MOVEMENT_DET;

import java.util.Date;

/**
 * @author nuno.nelas
 *
 */

import com.bezirk.middleware.messages.Event;

import language.I18N;

public class MovementUpdateEvent extends Event {

	private static final long serialVersionUID = 1L;

	private final Date time;

	public MovementUpdateEvent(Date time) {
		this.time = time;
	}

	public Date getTime() {
		return time;
	}

	public String toString() {
		return String.format(I18N.getString(MOVEMENT_DET) + "%s", time);
	}
}
