package events;
import static language.Messages.HUMIDITY_LEVEL;
import static language.Messages.DUST_LEVEL;
import static language.Messages.POLLEN_LEVEL;

import com.bezirk.middleware.messages.Event;

import language.I18N;

public class AirQualityUpdateEvent extends Event {

	private static final long serialVersionUID = 1L;

	private final double
		humidity /* decimal, e.g., 0.5 */,
		dustLevel /* milligrams per cubic meter. Above 20 is high. */,
		pollenLevel /* grams per cubic meter. Above 500 is high. */;

	public AirQualityUpdateEvent(double humidity, double dustLevel, double pollenLevel) {
		this.humidity = humidity;
		this.dustLevel = dustLevel;
		this.pollenLevel = pollenLevel;
	}

	public double getHumidity() {
		return humidity;
	}

	public double getDustLevel() {
		return dustLevel;
	}

	public double getPollenLevel() {
		return pollenLevel;
	}

	public String toString() {
		return String.format(I18N.getString(HUMIDITY_LEVEL) + ": %s, " + I18N.getString(DUST_LEVEL) + ": %s, " + I18N.getString(POLLEN_LEVEL) + ": %s", 
				humidity, dustLevel, pollenLevel);
	}
}
