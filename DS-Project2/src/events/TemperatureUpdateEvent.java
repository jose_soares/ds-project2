package events;

import com.bezirk.middleware.messages.Event;
import language.I18N;
import static language.Messages.TEMPERATURE;

public class TemperatureUpdateEvent extends Event{
	
	private static final long serialVersionUID = 1L;
	
	private final double temperature;

    public TemperatureUpdateEvent(double temperature) {
        this.temperature = temperature;
    }

    public double getTemperature() {
        return temperature;
    }

    public String toString() {
        return String.format(I18N.getString(TEMPERATURE) + ": %s", temperature);
    }
}

