package voice;

import com.sun.speech.freetts.VoiceManager;

import language.I18N;

import static language.Messages.TESTING;

public class Voice {
	final static String VOICENAME = "kevin";

	public static void talk(String message) {
		com.sun.speech.freetts.Voice voice;
		VoiceManager voiceManager = VoiceManager.getInstance();
		voice = voiceManager.getVoice(VOICENAME);
		voice.allocate();
		voice.speak(message);
	}

	/*public static void main(String[] args) {
		talk(I18N.getString(TESTING));
	}*/
}
