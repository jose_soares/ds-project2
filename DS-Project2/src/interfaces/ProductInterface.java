package interfaces;

import domain.Product;

public interface ProductInterface {

	void setupProduct();
	
	Product getProduct();
}
