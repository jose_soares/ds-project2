package interfaces;

import java.io.File;
import java.io.IOException;
import java.util.List;

import domain.Contact;

public interface ContactInterface {

	void loadContacts(File contactListFile) throws IOException;
	
	void createContact(Contact c) throws IOException;
	
	void addContactToFile(Contact contactToSave) throws IOException;
	
	List<Contact> getListContacts();
	
	void updateContactsFile() throws IOException;
	
	void deleteContact(int id) throws IOException;
}
