package interfaces;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import domain.PeriodicityEnum;
import domain.Warning;

public interface WarningInterface {

	void loadWarnings(File warningListFile) throws Exception;
	
	void createWarning(Warning w) throws IOException;
	
	void addWarningToFile(Warning warningToSave) throws IOException;
	
	List<Warning> getListWarnings();
	
	void updateWarningsFile() throws IOException;
	
	void deleteWarning(int id) throws IOException;

	SimpleDateFormat getSDF();
	
	PeriodicityEnum resolvePeriodicityEnum(String periodicity) throws Exception;
}
