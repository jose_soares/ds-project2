# IoT SmartHouse

## Software Product Line (SPL) of IoT applications.

## Authors:

- José Soares 44844 
- Nuno Nelas 51691
- Pedro Martins 52484
 
## Scope and summary

IoTSmartHouse is a project carried out under the masters degree in Computer Engineering @ FCUL.

This project consists on a software product line project that uses IoT devices to monitor temperature, air quality and even movement on a certain room. There's also features built into the project that are common to all products, such as creating warnings and send it to all users of a contact list. For helping users with special needs, we've implemented a TTS service (FreeTTS) and made use of the abilities of a SmartBulb.

All of our features were implemented with Bezirk Middleware API.

## Products 

### ProductA

Application with interface in Portuguese and suitable for blind and deaf users, with ability to control air quality.

### ProductB

Application With interface in English and suitable for blind users, with ability to detect movement.

### ProductC

Application with interface in Portuguese and suitable for blind users, able to detect movement and check the temperature.

### ProductD

Application with interface in English and suitable for deaf users, with ability to control air quality and check the temperature.

This was based on the use of aspects (AspectJ). This gave us the possibility to code features according to the type of product that the user want, giving a great variability to the product line.

## How to run

1. Import ds-project to your favorite IDE (we recomend Eclipse Java EE IDE for Web Developers, version 4.9.0).
2. Install AspectJ Development Tools and FeatureIDE. Both of them can be installed from Eclipse Marketplace. FeatureIDE is useful for checking our Feature Model.
3. Select one of the products (*.ajproperties*) and Apply Build Configuration.
4. Run Startup.java
5. Run corresponding sensor for mock purposes. 

Below is a list of sensors that a product use:

- ProductA - AirQualitySensorZirk.java
- ProductB - MovementSensorZirk.java
- ProductC - MovementSensorZirk.java, TemperatureSensorZirk.java
- ProductD - AirQualitySensorZirk.java, TemperatureSensorZirk.java